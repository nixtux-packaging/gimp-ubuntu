gmic (1:2.4.1-1) unstable; urgency=low

  * Version 2.4.1
  
 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Wed, 28 Nov 2018 03:02:00 +0300

gmic (1:2.3.4-4) unstable; urgency=low

  * Rebuild from https://launchpad.net/~otto-kesselgulasch/+archive/ubuntu/gimp
  
 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Tue, 21 Aug 2018 22:50:00 +0300
 
gmic (1:2.3.4+om-ubu18.04.1~ppa) bionic; urgency=high
  
  * Upstream release
  * https://discuss.pixls.us/t/on-the-road-to-2-4-0/8093/14
  * New plug-ins subfolder layout

 -- Otto Meier <otto@kesselgulasch.org>  Tue, 21 Aug 2018 17:10:00 +0200

gmic (1:1.8.0~pre-0y1~ppa~edge) yaketty; urgency=high
  
  * Prerelease: https://discuss.pixls.us/t/on-the-road-to-gmic-1-8-0/2758
  * Rebuild

 -- Otto Meier <otto@kesselgulasch.org>  Wed, 18 Jan 2016 21:15:00 +0100

gmic (1:1.7.9-0z0~ppa) zesty; urgency=high
  
  * New release: https://discuss.pixls.us/t/release-of-gmic-1-7-9/2497

 -- Otto Meier <otto@kesselgulasch.org>  Fri, 18 Nov 2016 20:15:00 +0100


gmic (1:1.7.3-0t2~ppa~edge) trusty; urgency=high
  
  * Upstream release

 -- Otto Meier <otto@kesselgulasch.org>  Sun, 03 Jul 2016 23:25:00 +0200

gmic (1:1.7.2-0p0~ppa) precise; urgency=high
  
  * Changelog: https://discuss.pixls.us/t/release-of-gmic-1-7-2/1348

 -- Otto Meier <otto@kesselgulasch.org>  Mon, 30 May 2016 22:38:00 +0200

gmic (1:1.6.5.9.100-0w1~ppa~edge) wily; urgency=high
  
  * Beta release
  * Try gcc-5 for wily
  * Changelog: https://discuss.pixls.us/t/on-the-road-to-gmic-1-6-6-0/383

 -- Otto Meier <otto@kesselgulasch.org>  Wed, 23 Sep 2015 01:40:34 +0200

gmic (1.5.7.1-0precise3~ppa) precise; urgency=high

  * Upstream Release
  * New features:
    - New substituting expression '@*' that returns the number of available cpus.
    - New choice "Auto" for parallel processing options in GIMP plug-in filters. 
    - Improved commands '-apply_parallel' and '-apply_parallel_overlap' for parallelization of algorithms.

  * Optimizations / Changes:
    - Command '-fft' now uses parallel computation to speed up the execution.
    - First command (and only the first) specified as an argument of '-parallel' is now able to modify global variables. 

 -- Otto Meier <otto@kesselgulasch.org>  Thu, 17 Sep 2013 16:51:34 +0200

gmic (1.5.7.0-0saucy1~ppa) saucy; urgency=high

  * Upstream Release

 -- Otto Meier <otto@kesselgulasch.org>  Tue, 10 Sep 2013 17:11:34 +0200


gmic (1.5.6.1-0precise0~ppa) precise; urgency=high
  
  * Upstream Beta Release
  * And here it is, another cool new version of the G'MIC framework :) Enjoy, and see you after the summer holidays :)
  
  * New features:
    - New filter 'Lights & Shadows / Drop shadow 3d' to create shadows layers projected on 3d planes
    - New filter 'Colors / Colorize comics' designed to help colorizing B&W sketches
    - New command '-pack_sprites', allows to do 'generic' shape fitting, i.e. render clouds of non-intersecting shapes with opt. masking. 
      Two new filters in the GIMP plug-in are using this generic command : 'Artistic / Shapeism' and 'Patterns / Pack sprites'
    - New command '-rprogress' to set a progress bar relatively to user-defined bounds. This allows progress bar to be coherent even when set by calls of sub-commands.
    
    Modifications:
    
    - Removed native commands '-haar' and '-ihaar' and recoded them as custom commands, with same parameters.
    - Re-organized the sections of the filters tree in the GIMP plug-in.
    - Zooming on a particular slice of a volumetric image in the embedded viewer now keeps displaying the z-coordinates of the explored voxels.
    
    Bug fixes:
    
    - Fixed : saving raw files in 'unsigned int' type mode were actually saving files in unsigned short instead [reponrted by Zonderr].
    - Fixed bug in command '-polygon' where list of vertices contained double points and opacity was <1.
    - Fixed : GIMP plug-in running in non-interactive mode now force output to be in-place. Was depending on the last selection (done in interactive mode).
    - Fixed : when running GIMP plug-in in non-interactive mode, progress bar works as expected.
     
 -- Otto Meier <otto@kesselgulasch.org>  Thu, 04 Jul 2013 09:54:34 +0200

gmic (1.5.5.2-0quantal2~ppa) quantal; urgency=high
  
  * Upstream Relase
  * This is a minor bugfixes release. Nothing outstanding, but better than previous version anyway :) Enjoy !
  
  * New features :
  
    - New filter 'Patterns / Seamless turbulence', which creates seamless turbulence patterns.
    - New command '-spline3d' to generate 3d cubic spline curves.
  * Improvements / Changes :
  
    - Slightly optimized blending step in patch-based inpainting algorithm.
    - Command '-spline' is now a custom command, instead of a native one.
    - Update mechanism for GIMP plug-in has been slightly changed. Only one update file is now required for all external contributions, which speeds up the update procedure dramatically.
    - Command '-output' better manages the saving of 'empty' images, by creating 0-bytes files in case the requested format does not support the storage of empty images (most of the formats actually).
    - Plug-in filters are now ordered in alphabetical order inside folders.
  
  * Bug fixes :
  
    - Loading single frame GIF files did not support alpha-channels. This has been fixed.
    -  Fixed bug happening when first displaying an image list with command '-window', modify its value range, then displaying it using command '-display'. Very rare bug to be honest!
    - Substituted expression @{ind,t} does not end a command line anymore, if image [ind] contains a 0.
    - Bugfix in '-object3d' : objects having sprites primitives with single colors and transparency masks were displayed at the wrong location. Thanks Jerome Boulanger for having noticed this! 
  
 -- Otto Meier <otto@kesselgulasch.org>  Fri, 19 Apr 2013 18:24:34 +0100
 
gmic (1.5.5.1-0quantal2~ppa) quantal; urgency=high
  
  * Upstream release
    
  * New features:
  
  * New command '-patches' can extract several patches from selected images, in a very quick way.
  * New command '-repair' to perform patch-based inpainting with an extra original blending algorithm. Very useful command!  
  * New external filter source from Jérome Boulanger, located at: https://raw.github.com/jboulanger/jboulanger-gmic/master/jboulanger.gmic
  
  * Modifications / improvements :
  
  * Blocks as '--local .. -endlocal' won't put a copy mark on images created inside the block.
  * Command '-arg' is now 10x faster, and accepts empty arguments.
  * Various optimizations have been performed on the default custom commands.
  * Options 'first_frame,last_frame,step_frame' are now accepted when loading multi-page .tif files.
  * Additional preview modes 'Duplicate horizontal' and 'Duplicate vertical' have been added for the GIMP plug-in.
  * Command '-warp' now accepts cubic interpolation in addition to existing interpolation modes 'nearest-neighbor' and 'linear'.
  * Parameters 'boundary' and 'interpolation' in command '-rotate' have been switched.
  * Inpainting filters can now choose the mask by color, instead of having an additional mask layer.
  
  * Bug fixes:
  
  * Command '-grid' was not working correctly when '0' was specified as the grid size along one of the x or y axes.
  * Command '-polygon' was buggy when drawing semi-transparent polygons with horizontal lines in it.
  * Command '-srand' has been fixed. Sometimes, it was not putting the right seed for the NRG. 
  
 -- Otto Meier <otto@kesselgulasch.org>  Fri, 29 Mar 2013 13:53:34 +0100

gmic (1.5.5.0-1quantal2~ppa) quantal; urgency=high
  
  * Upstream release
    
  * Changes:
  
  * Command '-richardson_lucy' has been renamed to '-deblur', as the implemented method was *not* the one from Richardson-Lucy, as Jérome B. pointed out to me.
  * Slight improvements of the debug messages, when using command '-debug'.
  * When trying to read a .pdf file as an image (using external call to 'convert'), the image resolution is now better.
  * Various optimizations of some native and custom commands have been done.
  
  * Bug fixes:
  
  * Fixed regression in 1.5.4.0 : saving animated gif was not working anymore.
  * Fixed regression in 1.5.4.0 : command '-gimp_list_filters' was not working anymore.
  * Fixed bug in -type' command which was preventing things like this working as expected : gmic -repeat 2 -type short -type float -done (Thanks to Jerome B. for having noticed this!).
  * Fixed bug in '-polygon'. Particular cases of complex polygons were not displaying correctly.
  * Fixed bug in PPM loader. PPM files generated by some tools by SONY were not readable.
  * Added additional memory checking to avoid segfault when incorrectly dealing with shared images. 
 
 -- Otto Meier <otto@kesselgulasch.org>  Sat, 9 Mar 2013 20:22:34 +0100

gmic (1.5.4.0-0quantal2~ppa) quantal; urgency=high

  * Upstream release
  
  * Improvements:
  
  * Recoded most of the layer blending modes. Now commands '-compose_*' are deprecated and most of them are replaced by a single command '-blend' [backported to 1.5.2.0+].
  * Removed all 'hardcoded' informations about locations for filters updates. Now, all is handled through the 'gmic_sources.cimgz' file.
  
  * Bug fixes:
  
  * Bash substituted characters (as `,!, etc..) are now accepted in filenames, when loading or saving image files with exotic formats (that request a call to external tool 'convert').
  * Fixed bug in command '-smooth'. The version taking a tensor field as a parameter and using LIC-based smoothing was not accepting a complete list of arguments.
  * Fixed bug in command '-image' (aka '-j'): The command was only considering the first channel of the opacity mask, when one was specified.
  * Fixed translation problem on Linux. Sometimes, the detected locale was incorrect. 
  
  * Misc
  
  * Change date from 2008 to 2013 in plugin info

 -- Otto Meier <otto@kesselgulasch.org>  Mon, 28 Jan 2013 20:50:34 +0100

gmic (1.5.3.0-0quantal4~ppa) quantal; urgency=high

  * Upstream release
  
  * New features:

  * Added command '-sort_str' to sort a list in the lexicographic order (where each image is assumed to be a string).
  * Pointer of image buffers now appear when printing image informations with the debug mode activated.
  * New commands '-echo_stdout' and '-echo_file' allowing to echo text on the standard output or in a specified filename.
  * New command '-gimp_list_filters' to output the list of all available G'MIC filters in the plug-in for GIMP.
  * New command '-symmetrize' and associated filters 'Deformations / Symmetrize' and 'Deformations / Symmetrizoscope'.

  * Bug fixes:

  * Wrong image indices were displayed on the log message when using '-fill_color', it has been fixed.
  * Fixed output of long argument names in log message.
  * Fixed small bug in TIFF loader, preventing some images to cause segfaults.
  * Fixed : Argument in command '-check' was not considering the last image as a reference when using variables, such as 'w','h','d','s'...
  * Fixed : External filter source files can now be hosted on https URL. 
  
  * Misc
  
  * Change date from 2008 to 2013 in plugin info

 -- Otto Meier <otto@kesselgulasch.org>  Thu, 10 Jan 2013 17:25:34 +0100

gmic (1.5.2.4-0quantal0~ppa) quantal; urgency=high

  * Upstream release

 -- Otto Meier <otto@kesselgulasch.org>  Sat, 08 Dec 2012 15:57:34 +0100

gmic (1.5.1.6+dfsg-4) unstable; urgency=low

  * [4ed5416f] Fix author/copyright information in debian/copyright and zart.1

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 04 Jul 2012 18:43:34 +0200

gmic (1.5.1.6+dfsg-3) unstable; urgency=low

  * [152547f5] Add missing Breaks/Replaces.

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 28 Jun 2012 19:55:47 +0200

gmic (1.5.1.6+dfsg-2) unstable; urgency=low

  * [4012fb0e] Split zart into its own package.
    This avoids pulling QT and X for a command-line tool.
  * [e420fdb0] Add a manpage for zart.
  * [eff81582] Remove debian/dirs to avoid empty directories.

 -- Bernd Zeimetz <bzed@debian.org>  Fri, 22 Jun 2012 18:12:19 +0200

gmic (1.5.1.6+dfsg-1) unstable; urgency=low

  * [cfe57a54] Merge tag 'upstream/1.5.1.6+dfsg'
    Upstream version 1.5.1.6+dfsg
  * [fbc1e4d2] Refreshing patches.

 -- Bernd Zeimetz <bzed@debian.org>  Fri, 22 Jun 2012 17:03:21 +0200

gmic (1.5.1.5+dfsg-1) unstable; urgency=low

  * [d3cb2da4] Remove override_dh_clean from .PHONY.
    Some debhelper versions still seem to recognize it as empty target...
  * [982453de] Merge commit 'upstream/1.5.0.9+dfsg'
  * [7c82f405] refreshing patches.
  * [7093fa88] Updating changelog.
  * [f8500c13] Merge tag 'upstream/1.5.1.0+dfsg'
    Upstream version 1.5.1.0+dfsg
  * [1e04612f] Rfreshing patches.
  * [ffd49557] Updating changelog.
  * [54eefb26] Merge tag 'upstream/1.5.1.5+dfsg'
    Upstream version 1.5.1.5+dfsg
  * [317788b2] Updating changelog.
  * [03467dc0] Refreshing patches.
  * [706f87a9] Backup zart/Makefile
  * [95621201] Add build-dependencies, description and install files for zart.
  * [eefe5f9b] Add patch to fix building with ne OpenCV.
  * [bdadee8c] Updating changelog.
  * [ccb14826] Add libopencv-objdetect-dev build-dependency.
  * [6b6322ef] More opencv fixes.
  * [303ff61c] Add libopencv-imgproc-dev as build-dependency.

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 21 Jun 2012 12:23:27 +0200

gmic (1.5.0.8+dfsg-1) unstable; urgency=low

  * [b7ddf3a2] Merge commit 'upstream/1.5.0.8+dfsg'
  * [a0dd17c6] Make GMIC build with recent OpenCV versions. (Closes: #652763)
  * [7cb4a04f] Don't use configure, call src/Makefile directly.

 -- Bernd Zeimetz <bzed@debian.org>  Sat, 21 Jan 2012 12:37:30 +0100

gmic (1.5.0.7+dfsg-1) unstable; urgency=low

  * [97e3ecbd] Merge commit 'upstream/1.5.0.7+dfsg'

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 04 Dec 2011 19:55:28 +0100

gmic (1.5.0.5+dfsg-1) unstable; urgency=low

  * [4978288c] Removing Jakub Wilk from Uploaders as requested.
    Thanks for your work Jakub!
  * [b7314e4e] Merge commit 'upstream/1.5.0.5+dfsg'
  * [866550ef] Refactoring debian/rules
  * [fce14609] Bumping Standards-Version, no changes needed.

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 30 Oct 2011 19:06:34 +0100

gmic (1.5.0.0+dfsg-1) unstable; urgency=low

  * [20fc4583] Merge commit 'upstream/1.5.0.0+dfsg'
  * [ec62b62d] Dropping patch for configure fixes, not necessary anymore.
  * [87daddc5] Refreshing patches.
  * [833f51b5] Remove lib*magick++*-dev build-deps, not needed anymore.
  * [7592f26e] Add libopenexr-dev as build-dependency.
  * [5a8d6059] Don't try to install upstream's changelog, not shipped anymore.

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 26 Jul 2011 15:17:46 +0200

gmic (1.4.8.1+dfsg-2) unstable; urgency=low

  * [24dc0e58] Add patch to fix FTBFS on various architectures.
    Thanks to Hector Oron (Closes: #614273)

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 20 Feb 2011 22:52:04 +0100

gmic (1.4.8.1+dfsg-1) unstable; urgency=low

  * [fab6cc4d] Merge commit 'upstream/1.4.8.1+dfsg'

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 16 Feb 2011 22:37:42 +0100

gmic (1.4.8.0+dfsg-1) unstable; urgency=low

  * [33847b44] Merge commit 'upstream/1.4.8.0+dfsg'
  * [6e771817] Refreshing patches.

 -- Bernd Zeimetz <bzed@debian.org>  Thu, 10 Feb 2011 13:13:22 +0100

gmic (1.4.4.2+dfsg-1) experimental; urgency=low

  * [3253ce08] Merge commit 'upstream/1.4.4.2+dfsg'

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 07 Nov 2010 18:47:00 +0100

gmic (1.4.2.0+dfsg-1) experimental; urgency=low

  * [f93deb46] Merge commit 'upstream/1.4.2.0+dfsg'
  * [00290d4f] Refreshing patches.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 20 Oct 2010 20:57:01 +0200

gmic (1.4.0.0+dfsg-1) experimental; urgency=low

  * [6fd356d5] Merge commit 'upstream/1.4.0.0+dfsg'
  * [331aef59] Adding new build-dependencies.
  * [86b49de0] Refreshing patches.

 -- Bernd Zeimetz <bzed@debian.org>  Fri, 03 Sep 2010 14:49:11 +0200

gmic (1.3.9.0+dfsg-1) unstable; urgency=low

  * [23ccb979] Merge commit 'upstream/1.3.9.0+dfsg'
  * [bef99d90] Install upstream changelog.

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 02 Aug 2010 10:26:51 +0200

gmic (1.3.7.1+dfsg-1) unstable; urgency=low

  * [b573582f] Merge commit 'upstream/1.3.7.1+dfsg'

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 19 Jul 2010 19:11:21 +0200

gmic (1.3.6.0+dfsg-1) unstable; urgency=low

  * [204cd64b] Merge commit 'upstream/1.3.6.0+dfsg'

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 30 Jun 2010 18:42:20 +0200

gmic (1.3.5.7+dfsg-1) unstable; urgency=low

  * [f733c366] Merge commit 'upstream/1.3.5.7+dfsg'
  * [ee1cba19] Refreshing patches.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 23 Jun 2010 02:23:16 +0200

gmic (1.3.5.4+dfsg-2) unstable; urgency=low

  * [e14a94e2] Rise required amount of RAM to avoid FTBFS on s390.
    (*sigh* - these machines should have neough RAM and swap....)

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 07 Jun 2010 19:37:02 +0200

gmic (1.3.5.4+dfsg-1) unstable; urgency=low

  * [821186d5] Fix dversionmangle in watch file.
  * [8fab3d8a] Merge commit 'upstream/1.3.5.4+dfsg'
    - This icludes a fix for the FTBFS on various archicetures.
      (Closes: #584304)
  * [7550bc35] Refresh patches.
  * [d682b862] Don't gunzip manpages and create directories before make
    install.
  * [6dccd71b] Tidy debian/rules' clean target.
  * [dba56977] Install bash completion

 -- Bernd Zeimetz <bzed@debian.org>  Mon, 07 Jun 2010 13:53:22 +0200

gmic (1.3.5.1+dfsg2-1) unstable; urgency=low

  * Upstream released a bugfix-release for 1.3.5.1 - unfortunately with the
    same version number.
  * [0e506788] Merge commit 'upstream/1.3.5.1+dfsg2'

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 26 May 2010 16:48:40 +0200

gmic (1.3.5.1+dfsg-1) unstable; urgency=low

  * [97366020] Use -O0 in case the system we build on has less than 1GB
    RAM.
  * [6aca1038] Merge commit 'upstream/1.3.5.1+dfsg'
  * [fe6b2057] Fix awk call to evaluate the available memory.
  * [6806172f] Updating patches.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 26 May 2010 12:13:50 +0200

gmic (1.3.5.0+dfsg-1) unstable; urgency=low

  * [4efc6ba8] Add Enhances/Depends on gimp.
  * [1cec005d] Merge commit 'upstream/1.3.5.0+dfsg'
  * [4a1816e8] Don't use pristine-tar on git-import-orig. We need to
    recreate the tarball after removing non-free material.

 -- Bernd Zeimetz <bzed@debian.org>  Sun, 16 May 2010 22:48:10 +0200

gmic (1.3.5.0~beta1+dfsg-1) experimental; urgency=low

  * Beta version of the upcoming upstream version.
  * [55bc031b] Merge commit 'upstream/1.3.5.0.beta1+dfsg'
  * [35c39744] Bumping Standards-Version to 3.8.4, no changes needed.
  * [1e7913f6] debian/control: Adding VCS Information.

 -- Bernd Zeimetz <bzed@debian.org>  Wed, 28 Apr 2010 19:49:35 +0200

gmic (1.3.4.1+dfsg-4) unstable; urgency=low

  * [973d62d8] Small typo making the kfreebsd fix fail. Fixed now.
  * [cd7344ee] Use -O0 on mips to avoid internal errors in g++.

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 20 Apr 2010 22:58:25 +0200

gmic (1.3.4.1+dfsg-3) unstable; urgency=low

  * [045a2f86] Remove strip commands from src/Makefile. dh_strip takes
    care of stripping if necessary.
  * [d3740bdf] Add patch to make gmic build on kfreebsd.
  * [61119501] Use -O0 on arm to stop g++ from using even more memory.

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 20 Apr 2010 18:09:57 +0200

gmic (1.3.4.1+dfsg-2) unstable; urgency=low

  * [77f87ac2] Update jwilk's email address to jwilk@d.o
  * [955493d8] Updating debian/copyright.
  * [6f168dd4] Add additional information about src/gmic_def.h to
    debian/copyright.

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 20 Apr 2010 11:15:26 +0200

gmic (1.3.4.1+dfsg-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Initial release (closes: #532629).

  [ Bernd Zeimetz ]
  * Removing non-free content (the html documentation comes without
    sources and was generated using a non-free album generator).
  * Also removing upstream's debian folder.

 -- Bernd Zeimetz <bzed@debian.org>  Tue, 13 Apr 2010 22:56:12 +0200

# GIMP 2.10 for Ubuntu 18.04 and 18.10 PPA sources

[ppa:mikhailnov/utils](https://launchpad.net/~mikhailnov/+archive/ubuntu/utils) 

**Full PPA documentation**: [https://gitlab.com/nixtux-packaging/utils-ppa/blob/master/README.md](https://gitlab.com/nixtux-packaging/utils-ppa/blob/master/README.md)

Packages are taken from [ppa:otto-kesselgulasch/gimp](https://launchpad.net/~otto-kesselgulasch/+archive/ubuntu/gimp) and slighly modified.

Additional patches to GIMP (ported from [ROSA](https://abf.io/gimp2102/gimp/tree/rosa2016.1) and modified):

* rename 'GNU Image Manipulation Tool' to 'GIMP'
* make it use 'Color' icons and 'System' GTK+ theme to keep default experience similar to 2.8 (no black theme and monochrome icons by default).

## Installation
```
sudo add-apt-repository ppa:mikhailnov/utils -y
sudo apt install gimp gimp-plugin-registry
```

## gimp-plugin-registry

I took it from Debian Sid and had to make some fixes to build it:

* `export CC=gcc` in debian/rules, otherwise `fix-ca` plugin tried to be built using missing `/usr/bin/gcc-6`
* switch from `automake` (1.15 and 1.16) to `automake1.11`
* add `gibgegl-dev` build dependency 

## Rebuilding PPA
Create a Launchpad PPA and run `./build-ppa.sh` after modifying PPA name in it.

## Rebuild locally
See [example](https://github.com/wwmm/pulseeffects/wiki/Installation-from-Source#debian--ubuntu).
